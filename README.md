# Beeline Test App

Тестовое приложение для Билайна.

## Установка

```sh
    yarn install
```

## Использование

Запустить

```sh
    npm run build:dev
```

потом открыть localhost:8080.

## Тесты

```sh
    npm test
    npm tslint
```
