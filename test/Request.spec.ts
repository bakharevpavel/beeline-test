import {} from 'mocha';
import {} from 'chai';

import { Request } from '../dev/app/common/Request';

describe("Request handling", () => {
    it("should get list of users", () => {
        Request.getUsers().then((users: Array<any>) => {
            chai.assert.isArray(users);
            chai.assert.isAbove(users.length, 0);
        });
    });

    it("should get user", () => {
        Request.getUser(1).then((user: Array<any>) => {
            chai.assert.exists(user);
        });
    });

    it("should delete user", () => {
        Request.deleteUser(1).then((user: Array<any>) => {
            chai.assert.exists(user);
        });
    });

    it("should update user", () => {
        Request.updateUser({
            id: 1,
            name: 'New name',
            address: 'New address',
            company: 'New company',
            email: 'New email',
            phone: 'New phone',
            username: 'New username',
            website: 'New website'
        }).then((user: Array<any>) => {
            chai.assert.exists(user);
        });
    });
});
