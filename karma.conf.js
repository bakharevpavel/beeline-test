module.exports = (config) => {
    config.set({
        frameworks: ['jasmine', 'mocha', 'chai'],

        files: [
            'test/**/*.spec.ts'
        ],

        preprocessors: {
            "test/**/*.spec.ts": ["webpack"]
        },

        browsers: ['Chrome'],

        mime: {
            'text/x-typescript': ['ts']
        },

        webpack: {
            module: {
                rules: [{
                    test: /\.ts$/,
                    loader: 'ts-loader'
                }, {
                    test: /\.css$/,
                    loader: 'null-loader'
                }, {
                    test: /\.(jade|pug)$/,
                    loader: 'pug-loader'
                }, {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader?name=img/[name].[ext]'
                }, {
                    test: /\.(eot|svg|ttf|woff|woff2)$/,
                    loader: 'file-loader?name=fonts/[name].[ext]'
                }, {
                    test: /\.less$/,
                    loader: 'null-loader'
                }]
            },
            resolve: {
                extensions: ['*', '.ts', '.js', '.less']
            },
            mode: 'development'
        }
    });
};
