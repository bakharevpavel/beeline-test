export interface IUser {
    id: number;
    address: Object;
    company: Object;
    email: string;
    name: string;
    phone: string;
    username: string;
    website: string;
};
