import { IUser } from './Interfaces';

export class Request {

    private static USER_ADDRESS: string = 'https://jsonplaceholder.typicode.com/users';

    public static deleteUser(id: number) {
        return fetch(`${this.USER_ADDRESS}/${id}`, {
            method: 'DELETE',
        }).then((response: any) => response.json());
    };

    public static getUser(id: number) {
        return fetch(`${this.USER_ADDRESS}/${id}`).then((response: any) => response.json());
    };

    public static getUsers(offset: number = 0, limit: number = 10) {
        let url: URL = new URL(this.USER_ADDRESS);
        url.searchParams.append('offset', `${offset}`);
        url.searchParams.append('limit', `${limit}`);
        return fetch(url.toString()).then((response: any) => response.json());
    };

    public static updateUser(user: IUser) {
        return fetch(`${this.USER_ADDRESS}/${user.id}`, {
            method: 'PUT',
            body: JSON.stringify(user)
        }).then((response: any) => response.json());
    };
};
