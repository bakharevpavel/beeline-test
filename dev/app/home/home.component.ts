import { Component } from '@angular/core';
import { Request } from '../common/Request';
import { IUser } from '../common/Interfaces';

@Component({
    selector: 'home-component',
    template: require('./home.component.pug')()
})
export class HomeComponent {
    users: Array<IUser> = [];

    constructor() {
        Request.getUsers().then((response: IUser[]) => {
            if(!response) {
                throw new Error('Get request error.');
            };

            this.users = response;
        }).catch((err) => {
            throw err;
        });
    };

    remove(index: number) {
        Request.deleteUser(this.users[index].id).then((response: any) => {
            if(!response) {
                throw new Error('Delete request error.');
            };

            this.users.splice(index, 1);
        }).catch((err: any) => {
            throw err;
        });
    };
};
