import { Component } from '@angular/core';
import '../../node_modules/@angular/material/prebuilt-themes/indigo-pink.css';
import '../../node_modules/material-design-icons/iconfont/material-icons.css';
import './app.component.less';

@Component({
    selector: 'beeline-app',
    template: require('./app.component.pug')()
})
export class AppComponent {};
