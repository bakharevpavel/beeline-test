import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { IUser } from '../common/Interfaces';
import { Request } from '../common/Request';
import { NotifyDialogComponent } from '../notify/notify.component';

@Component({
    selector: 'user-component',
    template: require('./user.component.pug')()
})
export class UserComponent implements OnInit, OnDestroy {

    private sub: any;

    public user: IUser = null;

    constructor(private route: ActivatedRoute, public dialog: MatDialog) {};

    ngOnDestroy() {
        this.sub.unsubscribe();
    };

    ngOnInit() {
        this.sub = this.route.params.subscribe((params: any) => {
            Request.getUser(params.id).then((user: IUser) => {
                this.user = user;
            });
        });
    };

    save() {
        Request.updateUser(this.user).then((response: any) => {
            if(!response) {
                throw new Error('Update request error.');
            };

            this.dialog.open(NotifyDialogComponent, { data: { msg: 'User succesfully updated!' } });
        }).catch((err: Error) => {
            this.dialog.open(NotifyDialogComponent, { data: { msg: 'User update error!' } });
            throw err;
        });
    };
};
