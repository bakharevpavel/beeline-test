import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { MatButtonModule, MatInputModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { routing, routingProviders } from './app.routing';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material';
import { NotifyDialogComponent } from './notify/notify.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        UserComponent,
        NotifyDialogComponent
    ],
    entryComponents: [
        NotifyDialogComponent
    ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        routing,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatListModule,
        MatCardModule,
        MatDialogModule,
        MatGridListModule,
        FlexLayoutModule
    ],
    providers: [routingProviders],
    bootstrap: [AppComponent]
})

export class AppModule {

    constructor(public appRef: ApplicationRef) {};

    hmrOnInit(store: any) {
        if(!store || !store.state) {
            return;
        };

        if('restoreInputValues' in store) {
            store.restoreInputValues();
        };

        this.appRef.tick();

        delete store.state;
        delete store.restoreInputValues;
    };

    hmrOnDestroy(store: any) {
        let cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);

        store.disposeOldHosts = createNewHosts(cmpLocation);

        store.state = {};

        store.restoreInputValues  = createInputTransfer();

        removeNgStyles();
    };

    hmrAfterDestroy(store: any) {
        store.disposeOldHosts()
        delete store.disposeOldHosts;
    };
};
