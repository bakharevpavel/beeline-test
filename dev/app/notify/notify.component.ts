import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'notify-dialog',
  template: require('./notify.component.pug')(),
})
export class NotifyDialogComponent {

    constructor(public dialogRef: MatDialogRef<NotifyDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {};

    onOkClick(): void {
        this.dialogRef.close();
    };
};
