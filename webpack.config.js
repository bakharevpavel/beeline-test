const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = env => {
    let plugins = [
        new webpack.NoEmitOnErrorsPlugin(),
        new ExtractTextPlugin('[name].css'),
        new HtmlWebpackPlugin({
            template: 'dev/index.pug'
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(env.type == 'prod')
        })
    ];

    return {
        devServer: {
            contentBase: 'http://localhost:8080/',
            hot: true
        },

        entry: {
            polyfills: './dev/polyfills.ts',
            vendor: './dev/vendor.ts',
            app: './dev/main.ts'
        },

        resolve: {
            extensions: ['*', '.ts', '.js', '.less']
        },

        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loader: 'ts-loader'
                },            {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' })
                },
                {
                    test: /\.(jade|pug)$/,
                    loader: 'pug-loader'
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    exclude: /node_modules/,
                    loader: 'file-loader?name=img/[name].[ext]'
                }, {
                    test: /\.(eot|svg|ttf|woff|woff2)$/,
                    loader: 'file-loader?name=fonts/[name].[ext]'
                },
                {
                    test: /\.less$/,
                    loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!less-loader' })
                }
            ]
        },

        devtool: 'source-map',

        output: {
            path: __dirname  + '/output/',
            publicPath: env.type == 'dev' ? '/' : './',
            filename: '[name].js',
            chunkFilename: '[id].chunk.js'
        },

        optimization: {
          splitChunks: {
            cacheGroups: {
              vendor: {
                chunks: 'all',
                test: /[\\/]node_modules[\\/]/,
                name: 'vendor',
                enforce: true
              }
            }
          }
        },

        plugins: env.type == 'dev' ? plugins.concat([
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NamedModulesPlugin()
        ]) : plugins,

        watch: (env.type == 'dev')
    };
};
